﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YodaFileUpload.Controllers
{
    public class UploadController : Controller
    {
        private const string _uploadFolder = "~/Uploads";

        [HttpGet]
        public void Upload()
        {
            var queryString = Request.QueryString;
            if (queryString.Count == 0) return;

            // Read parameters
            var uploadToken = queryString.Get("upload_Token");
            int resumableChunkNumber = int.Parse(queryString.Get("resumableChunkNumber"));

            var filePath = string.Format("{0}/{1}/{1}.part{2}", _uploadFolder, uploadToken, resumableChunkNumber.ToString("0000"));
            var localFilePath = Server.MapPath(filePath);

            // Response
            var fileExists = System.IO.File.Exists(localFilePath);
            if (fileExists)
            {
                //return a 200 so the page knows the chunk has been uploaded
                Response.StatusCode = 200;
            }
            else
            {
                //return a 204 to say that they request was recived but no file was found
                //you can also return a 404 here but if you dont like console errors for each test request that fails 
                //rather return a 204
                Response.StatusCode = 204;
            }
        }


        [HttpPost]
        public void Upload(object data)
        {
            var form = Request.Form;
            if (form.Count == 0) return;

            // Read parameters
            var uploadToken = form.Get("upload_Token");
            int resumableChunkNumber = int.Parse(form.Get("resumableChunkNumber"));
            var resumableFilename = form.Get("resumableFilename");
            var resumableIdentifier = form.Get("resumableIdentifier");
            int resumableChunkSize = int.Parse(form.Get("resumableChunkSize"));
            long resumableTotalSize = long.Parse(form.Get("resumableTotalSize"));

            var filePath = string.Format("{0}/{1}/{1}.part{2}", _uploadFolder, resumableIdentifier, resumableChunkNumber.ToString("0000"));
            var localFilePath = Server.MapPath(filePath);
            var directory = System.IO.Path.GetDirectoryName(localFilePath);
            if (!System.IO.Directory.Exists(localFilePath)) System.IO.Directory.CreateDirectory(directory);

            //There should only be a single file per request
            if (Request.Files.Count == 1)
            {
                // save chunk
                if (!System.IO.File.Exists(localFilePath))
                {
                    Request.Files[0].SaveAs(localFilePath);
                }

                // Check if all chunks are ready and save file
                var files = System.IO.Directory.GetFiles(directory);
                if ((files.Length + 1) * (long)resumableChunkSize >= resumableTotalSize)
                {
                    filePath = string.Format("{0}/{1}{2}", _uploadFolder, resumableIdentifier, System.IO.Path.GetExtension(resumableFilename));

                    localFilePath = Server.MapPath(filePath);
                    using (var fs = new FileStream(localFilePath, FileMode.CreateNew))
                    {
                        foreach (string file in files.OrderBy(x => x))
                        {
                            var buffer = System.IO.File.ReadAllBytes(file);
                            fs.Write(buffer, 0, buffer.Length);
                            System.IO.File.Delete(file);
                        }
                    }
                    System.IO.Directory.Delete(directory);

                    string newFileName = string.Format("{0}/{1}", _uploadFolder, resumableFilename);

                    if (System.IO.File.Exists(Server.MapPath(newFileName)))
                    {
                        System.IO.File.Delete(Server.MapPath(newFileName));
                    }
                    System.IO.File.Move(Server.MapPath(filePath), Server.MapPath(newFileName));
                }
            }
        }
    }
}
